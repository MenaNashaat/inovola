
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import axios from "axios";
import mainComponent from "./plugins/mainComponent";
import './registerServiceWorker'

Vue.config.productionTip = false;
Vue.use(mainComponent);
new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
