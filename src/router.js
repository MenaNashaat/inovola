import Vue from "vue";
import Router from "vue-router";
import AppHeader from "./layout/AppHeader";
import AppFooter from "./layout/AppFooter";
import Components from "./views/Components.vue";
import apps from "./views/apps.vue";
import develop from "./views/develop.vue";

Vue.use(Router);

export default new Router({
  linkExactActiveClass: "active",
  routes: [
    {
      path: "/",
      name: "components",
      components: {
        header: AppHeader,
        default: Components,
        footer: AppFooter
      }
    },
    
    {
      path: "/apps",
      name: "apps",
      components: {
        header: AppHeader,
        default: apps,
        footer: AppFooter
      }
    },
    {
      path: "/develop",
      name: "develop",
      components: {
        header: AppHeader,
        default: develop,
        footer: AppFooter
      }
    },
    
  ],
  scrollBehavior: to => {
    if (to.hash) {
      return { selector: to.hash };
    } else {
      return { x: 0, y: 0 };
    }
  }
});
